#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QColorDialog>
#include "ndifinder.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void openColorPicker();
    void upBtnClicked(bool checked);
    void downBtnClicked(bool checked);
    void refreshSourcesClicked(bool checked);
    void sourcesComboChanged(int index);

private:
    QColorDialog colorPicker;
    NDIFinder ndiFinder;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
