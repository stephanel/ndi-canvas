#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->drawZone->setBrushWidth(ui->brushWidth->value());

    connect(ui->brushWidth, SIGNAL(valueChanged(int)), ui->drawZone, SLOT(setBrushWidth(int)));
    connect(ui->clearBtn, SIGNAL(clicked(bool)), ui->drawZone, SLOT(clearImage()));
    connect(ui->colorBtn, SIGNAL(clicked(bool)), this, SLOT(openColorPicker()));

    connect(ui->upSizeBtn, SIGNAL(clicked(bool)), this, SLOT(upBtnClicked(bool)));
    connect(ui->downSizeBtn, SIGNAL(clicked(bool)), this, SLOT(downBtnClicked(bool)));

    connect(ui->refreshSourcesBtn, SIGNAL(clicked(bool)), this, SLOT(refreshSourcesClicked(bool)));
    connect(ui->sourcesCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(sourcesComboChanged(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openColorPicker()
{
    colorPicker.open(ui->drawZone, SLOT(setBrushColor(QColor)));
}

void MainWindow::upBtnClicked(bool checked)
{
    int value = ui->brushWidth->value();
    ui->brushWidth->setValue(value+2);
}

void MainWindow::downBtnClicked(bool checked)
{
    int value = ui->brushWidth->value();
    ui->brushWidth->setValue(value-2);
}

void MainWindow::refreshSourcesClicked(bool checked)
{
    QList<NDIlib_source_t> sources = ndiFinder.FindSources();

    ui->sourcesCombo->clear();
    Q_FOREACH(NDIlib_source_t source, sources)
    {
        QVariant itemData(QString(source.p_ip_address));
        ui->sourcesCombo->addItem(QString(source.p_ndi_name), itemData);
    }
}

void MainWindow::sourcesComboChanged(int index)
{
    QString sourceName = ui->sourcesCombo->itemText(index);
    QString sourceIp = ui->sourcesCombo->itemData(index).toString();

    if(!sourceName.isEmpty() && !sourceIp.isEmpty())
    {
        QByteArray srcNameData = sourceName.toUtf8();
        QByteArray srcIpData = sourceIp.toUtf8();

        NDIlib_source_t newSource = {0};
        newSource.p_ndi_name = srcNameData.constData();
        newSource.p_ip_address = srcIpData.constData();

        ui->drawZone->SetNDISource(newSource);
    }
}
