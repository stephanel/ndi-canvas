#include "drawarea.h"
#include <QPalette>

DrawArea::DrawArea(QWidget *parent) : QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);

    _modified = false;
    _drawing = false;
    _brushWidth = 1;
    _brushColor = Qt::red;

    _img = QImage(QSize(1280, 720), QImage::Format_RGBA8888_Premultiplied);
    _img.fill(Qt::transparent);

    _ndiBackground = QImage(_img);

    ndiIn = new NDIReceiver(Q_NULLPTR);
    connect(ndiIn, SIGNAL(videoFrameReceived(QImage)), this, SLOT(ndiImageReceived(QImage)));
    ndiIn->start();

    ndiOut = new NDISender("ndi-canvas", &_img);
    ndiOut->start();
}

void DrawArea::setBrushWidth(int newWidth)
{
    _brushWidth = newWidth;
}

void DrawArea::setBrushColor(const QColor &newColor)
{
    _brushColor = newColor;
}

void DrawArea::clearImage()
{
    _img.fill(Qt::transparent);
    _modified = true;
    update();
}

QPoint DrawArea::mapToImage(QPoint p)
{
    QRect imgRect = _scaledImg.rect();
    int xOffset = this->rect().center().x() - imgRect.center().x();
    int yOffset = this->rect().center().y() - imgRect.center().y();

    QRect drawRect(imgRect);
    drawRect.adjust(xOffset, yOffset, xOffset, yOffset);

    int x = p.x() - drawRect.left();
    int y = p.y() - drawRect.top();

    if(x < 0)
        x = 0;
    if(x > _scaledImg.width())
        x = _scaledImg.width();

    if(y < 0)
        y = 0;
    if(y > _scaledImg.height())
        y = _scaledImg.height();

    double xScale = (double)(_img.width()) / (double)(_scaledImg.width());
    double yScale = (double)(_img.height()) / (double)(_scaledImg.height());

    int newX = (x * (xScale * 100)) / 100;
    int newY = (y * (yScale * 100)) / 100;

    return QPoint(newX, newY);
}

void DrawArea::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        _lastPoint = mapToImage(event->pos());
        _drawing = true;
    }
}

void DrawArea::mouseMoveEvent(QMouseEvent *event)
{
    if((event->buttons() & Qt::LeftButton) && _drawing)
        drawLineTo(mapToImage(event->pos()));
}

void DrawArea::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton && _drawing)
    {
        drawLineTo(mapToImage(event->pos()));
        _drawing = false;
    }
}

void DrawArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    _scaledImg = _img.scaled(size(), Qt::KeepAspectRatio, Qt::FastTransformation);

    QRect imgRect = _scaledImg.rect();
    int xOffset = this->rect().center().x() - imgRect.center().x();
    int yOffset = this->rect().center().y() - imgRect.center().y();

    QRect drawRect(imgRect);
    drawRect.adjust(xOffset, yOffset, xOffset, yOffset);

    painter.fillRect(drawRect, Qt::black);
    painter.drawImage(drawRect, _ndiBackground, _ndiBackground.rect());
    painter.drawImage(drawRect, _scaledImg, imgRect);
}

void DrawArea::resizeEvent(QResizeEvent *event)
{
    update(rect());
    QWidget::resizeEvent(event);
}

void DrawArea::drawLineTo(const QPoint &endPoint)
{
    QPainter painter(&_img);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);

    painter.setPen(QPen(_brushColor, _brushWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawLine(_lastPoint, endPoint);
    _modified = true;

    update(rect());

    _lastPoint = endPoint;
}

void DrawArea::SetNDISource(NDIlib_source_t source)
{
    ndiIn->SetSource(source);
}

void DrawArea::ndiImageReceived(const QImage frame)
{
    _ndiBackground = frame.scaled(_img.size(), Qt::KeepAspectRatio, Qt::FastTransformation);
    update();
}
