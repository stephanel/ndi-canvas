#-------------------------------------------------
#
# Project created by QtCreator 2017-03-06T14:32:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ndi-canvas
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(release, debug|release): BINDIR = "$$OUT_PWD\release"
CONFIG(debug, debug|release): BINDIR = "$$OUT_PWD\debug"

NDISDK_DIR = "C:\Program Files\NewTek\NewTek NDI SDK"

SOURCES += main.cpp\
        mainwindow.cpp \
    drawarea.cpp \
    ndifinder.cpp \
    ndisender.cpp \
    ndireceiver.cpp

HEADERS  += mainwindow.h \
    drawarea.h \
    ndifinder.h \
    ndisender.h \
    ndireceiver.h

FORMS    += mainwindow.ui

INCLUDEPATH += "$$NDISDK_DIR\Include"

win32 {
    !contains(QMAKE_TARGET.arch, x86_64) {
        # 32bit
        LIBS += "$$NDISDK_DIR\Lib\x86\Processing.NDI.Lib.x86.lib"
        QMAKE_PRE_LINK += "copy /Y \"$$NDISDK_DIR\Bin\x86\Processing.NDI.Lib.x86.dll\" \"$$BINDIR\Processing.NDI.Lib.x86.dll\" "
    } else {
        # 64bit
        LIBS += "$$NDISDK_DIR\Lib\x64\Processing.NDI.Lib.x64.lib"
        QMAKE_PRE_LINK += "copy /Y \"$$NDISDK_DIR\Bin\x64\Processing.NDI.Lib.x64.dll\" \"$$BINDIR\Processing.NDI.Lib.x64.dll\" "
    }
}
