#include "ndireceiver.h"
#include <QImage>
#include <QDebug>

NDIReceiver::NDIReceiver(QObject *parent) :
    QThread(parent),
    recvMutex(QMutex::NonRecursive)
{
}

NDIReceiver::~NDIReceiver()
{
    NDIlib_recv_destroy(recvInstance);
}

void NDIReceiver::SetSource(NDIlib_source_t source)
{
    recvMutex.lock();

    if(recvInstance)
        NDIlib_recv_destroy(recvInstance);

    NDIlib_recv_create_t recvDesc = {0};
    recvDesc.source_to_connect_to = source;
    recvDesc.bandwidth = NDIlib_recv_bandwidth_lowest;
    recvDesc.allow_video_fields = false;
    recvDesc.color_format = NDIlib_recv_color_format_e_RGBX_RGBA;

    qDebug() << "starting receiver for source " << source.p_ndi_name;
    recvInstance = NDIlib_recv_create2(&recvDesc);

    recvMutex.unlock();
}

void NDIReceiver::run()
{
    NDIlib_video_frame_t videoFrame = {0};
    NDIlib_frame_type_e frameType = NDIlib_frame_type_none;

    while(true)
    {
        if(!recvInstance)
        {
            qDebug() << "No NDI receiver instance available. Waiting before retry...";
            this->usleep(1000);
            continue;
        }

        recvMutex.lock();
        frameType = NDIlib_recv_capture(recvInstance, &videoFrame, NULL, NULL, 1);
        recvMutex.unlock();

        if(frameType == NDIlib_frame_type_video)
        {
            const QImage frame(videoFrame.p_data, videoFrame.xres, videoFrame.yres, QImage::Format_RGBA8888_Premultiplied);
            emit videoFrameReceived(frame.copy());

            NDIlib_recv_free_video(recvInstance, &videoFrame);
        }
    }
}
