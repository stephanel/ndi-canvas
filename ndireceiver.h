#ifndef NDIRECEIVER_H
#define NDIRECEIVER_H

#include <QThread>
#include <QMutex>
#include <Processing.NDI.Lib.h>

class NDIReceiver : public QThread
{
    Q_OBJECT
public:
    explicit NDIReceiver(QObject *parent = 0);
    ~NDIReceiver();
    void SetSource(NDIlib_source_t source);

signals:
    void videoFrameReceived(const QImage &frame);

private:
    NDIlib_recv_instance_t recvInstance;
    QMutex recvMutex;
    void run();
};

#endif // NDIRECEIVER_H
