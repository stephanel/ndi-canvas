#ifndef DRAWAREA_H
#define DRAWAREA_H

#include <QWidget>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QImage>
#include <QPoint>
#include <QColor>
#include "ndisender.h"
#include "ndireceiver.h"

namespace UI{
class DrawArea;
}

class DrawArea : public QWidget
{
    Q_OBJECT

public:
    explicit DrawArea(QWidget *parent = 0);
    bool isModified() const { return _modified; }
    QColor brushColor() const { return _brushColor; }
    int brushWidth() const { return _brushWidth; }
    void SetNDISource(NDIlib_source_t source);

public slots:
    void setBrushColor(const QColor &newColor);
    void setBrushWidth(int newWidth);
    void clearImage();

private slots:
    void ndiImageReceived(const QImage frame);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    void drawLineTo(const QPoint &endPoint);
    QPoint mapToImage(QPoint p);

    bool _modified;
    bool _drawing;
    int _brushWidth;
    QColor _brushColor;
    QImage _img;
    QImage _scaledImg;
    QImage _ndiBackground;
    QPoint _lastPoint;

    NDIReceiver *ndiIn;
    NDISender *ndiOut;
};

#endif // DRAWAREA_H
