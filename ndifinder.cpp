#include "ndifinder.h"

NDIFinder::NDIFinder(QObject *parent) : QObject(parent)
{
    NDIlib_find_create_t findDesc = {0};
    findDesc.show_local_sources = true;
    findDesc.p_groups = NULL;
    findInstance = NDIlib_find_create(&findDesc);
}

NDIFinder::~NDIFinder()
{
    NDIlib_find_destroy(findInstance);
}

QList<NDIlib_source_t> NDIFinder::FindSources()
{
    QList<NDIlib_source_t> list;

    uint32_t sourceCount = 0;
    const NDIlib_source_t *sources = NDIlib_find_get_current_sources(findInstance, &sourceCount);

    for(uint32_t i = 0; i < sourceCount; i++)
    {
        list.push_back(sources[i]);
    }

    return list;
}
